package uz.shuxrat;

import java.util.Scanner;

public class Task2 {
    //sonlar ko'paytmasini nechta nol bilan tugashini topish uchun 2 va 5 ko'paytma juftliklarini aniqlash
    //yetarli. Xotirani tejash maqsadida masala shartidan kelib chiqib ushbu optimal yechim taqsim etildi.
    //masalani 2- yechimixam mavjud. Ularni har bittasini yozib chiqib, ko'paytrib topsaxam bo'ladi lekin undan ma'no yoq!!!
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Son qiymat kiriting: ");
        int a = sc.nextInt();
        if (a <= 1) {
            if (a<0){
                System.out.println("Bu son manfiy son");
            }else {
                System.out.println("Bu son tub son emas");
            }
        } else if (a < 5) {
            System.out.println("1 va " + a + " oralig'dagi tub sonlar ko'paytmasi 0 bilan tugamaydi");
        } else {
            System.out.println("1 va " + a + " oralig'dagi tub sonlar ko'paytmasi 1 ta 0 bilan tugaydi");
        }
        main(args);
    }



}
